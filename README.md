# sisop-praktikum-modul-2-2023-WS-F04
# Praktikum Sistem Operasi Modul 2
# Kelompok F04
- Fathan Abi Karami 5025211156
- Heru Dwi Kurniawan 5025211055
- Alya Putri Salma 5025211174

# Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

## Poin A
Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

```bash
int main(){
    int stat_download;
    pid_t Downloadid = fork();
    
    if (Downloadid < 0){
        exit(EXIT_FAILURE);
    }
    if (Downloadid == 0){
        char *argv[] = {"wget", "-qO", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "&", NULL};
        execv("/usr/bin/wget", argv);
    }
    waitpid(Downloadid, &stat_download, 0);
```
Penjelasan: 

`int stat_download;`: digunakan untuk mendeklarasikan  variabel stat_download yang bertipe data integer.

`pid_t Downloadid = fork();`: mendeklarasikan bahwa variabel Downloadid bertipe data `pid_t` yang digunakan untuk menyimpan ID proses, kemudian menginisialisasikannya dengan fungsi `fork()`.

`if (Downloadid < 0)`:mengecek apakah child process dengan variabel Downloadid berhasil atau tidak. Jika kurang dari nol, maka child process gagal dengan status `EXIT_FAILURE`, sehingga output program akan error.

`if (Downloadid == 0)`:mengeceka apakah variabel Downloadid bernilai nol. Jika nol berarti child process berhasil dilakukan.
`
```bash 
char *argv[] = {"wget", "-qO", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "&", NULL};
```
Merupakan perintah untuk men-download file. Dengan code `execv("/usr/bin/wget", argv);` digunakan untuk menjalankan program wget dengan argumen argv.

`waitpid(Downloadid, &stat_download, 0);`:menunggu hingga proses download selesai dijalankan dan menyimpan statusnya ke dalam variabel stat_download.

## Poin B 
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
```bash
    int stat_unzip;
    pid_t unzipid = fork();
    
    if(unzipid == 0){
        char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    waitpid(unzipid, &stat_unzip, 0);
```
Penjelasan:

`int stat_unzip;`:digunakan untuk mendeklarasikan  variabel stat_unzip yang bertipe data integer.

`pid_t unzipid = fork();`:mendeklarasikan bahwa variabel unzipid bertipe data `pid_t` yang digunakan untuk menyimpan ID process, kemudian menginisialisasikannya dengan fungsi `fork()`.

`if(unzipid == 0)`:untuk melakukan pengecekan apakah variabel unzipID bernilai nol atau tidak. Jika nol maka proses tersebut merupakan child process.

`waitpid(unzipid, &stat_unzip, 0);`:menunggu hingga proses unzip selesai dijalankan dan menyimpan statusnya ke dalam variabel stat_unzip.
```bash
char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
        execv("/usr/bin/unzip", argv);
```
Merupakan perintah untuk melakukan unzip file. Dengan code `execv("/usr/bin/wget", argv);` digunakan untuk menjalankan program wget dengan argumen argv.


## Poin C 
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

- Memilih secara acak file gambar untuk melakukan shift penjagaan pada hewan tersebut menggunakan kode berikut:

```bash
    char dir_path[]= ".";
    struct dirent *dp;
    char *path[100];
    DIR *dir = opendir(dir_path);
    
    if (!dir)
      return 0;

    int i = 0;
     while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
        if (dp->d_type == DT_REG)
        {
          char*token = dp->d_name;
          path[i] = token;
          i++;
        }
    }
    }


        // printf("%ld -> %s -> %ld -> %d -> %d\n", dp->d_ino, dp->d_name, dp->d_off, dp->d_reclen, dp->d_type);
    
    int pilih = rand() % sizeof(*path);

    printf("Hewan Yang Dijaga : %s \n", strtok(path[pilih], "."));
    FILE *file;
    file = fopen("penjaga.txt", "w");
    fprintf(file, "Hewan Yang Dijaga : %s", strtok(path[pilih], "."));
    fclose(file);


    closedir(dir);
```

- Melakukan pengelompokkan hewan darat, air dan amphibi dan melakukan zip pada tiap-tiap foldernya.
```bash

    // ==GRUP===================================

    pid_t grupid;
    int status_grup;

    grupid = fork();

    if (grupid < 0) {
        exit(EXIT_FAILURE);
    }

    if (grupid == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/mkdir", argv);
    }
    waitpid(grupid, &status_grup, 0);

    // ==PEMILAHAN===================================
    
    DIR *dirr = opendir(".");
    pid_t pilahid;
    int status_pilah;

    while ((dp = readdir(dirr)) != NULL) {
                
        if (dp->d_type == DT_REG) {
            if (strstr(dp->d_name, "darat") != NULL) {
                pilahid = fork();

                if (pilahid < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilahid == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanDarat", NULL};
                    execv("/bin/mv", argv);
                }
                waitpid(pilahid, &status_pilah, 0);
            }

            if (strstr(dp->d_name, "amphibi") != NULL) {
                pilahid = fork();

                if (pilahid < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilahid == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanAmphibi", NULL};
                    execv("/bin/mv", argv);
                }
                waitpid(pilahid, &status_pilah, 0);
            }

            if (strstr(dp->d_name, "air") != NULL) {
                pilahid = fork();

                if (pilahid < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilahid == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanAir", NULL};
                    execv("/bin/mv", argv);
                }
                waitpid(pilahid, &status_pilah, 0);
            }
        }
    }
    closedir(dirr);
```

## Poin D 
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system
```bash
    pid_t zipid;
    int status_zip;

    // 1. Darat
    zipid = fork();

    if (zipid < 0) {
        exit(EXIT_FAILURE);
    }

    if (zipid == 0) {
        char *argv[] = {"zip", "-qr", "HewanDarat.zip", "HewanDarat", NULL};
        execv("/bin/zip", argv);
    }
    waitpid(zipid, &status_zip, 0);

    // 2. Amphibi
    zipid = fork();

    if (zipid < 0) {
        exit(EXIT_FAILURE);
    }

    if (zipid == 0) {
        char *argv[] = {"zip", "-qr", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/bin/zip", argv);
    }
    waitpid(zipid, &status_zip, 0);

    // 3. Air
    zipid = fork();

    if (zipid < 0) {
        exit(EXIT_FAILURE);
    }

    if (zipid == 0) {
        char *argv[] = {"zip", "-qr", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
    }
    waitpid(zipid, &status_zip, 0);
```
Penjelasan:

`int status_zip;`:digunakan untuk mendeklarasikan  variabel yang bertipe data integer.

`pid_t zipid = fork();`:mendeklarasikan bahwa variabel zipid bertipe data `pid_t` yang digunakan untuk menyimpan ID process, kemudian menginisialisasikannya dengan fungsi `fork()`.

`waitpid(zipid, &status_zip, 0);`:menunggu hingga proses unzip selesai dijalankan dan menyimpan statusnya ke dalam variabel status_zip.

- Melakukan delete setelah foolder-folder yang ada dicompress menjadi zip, menggunakan kode sebagai berikut:
```bash
    pid_t delete_id;
    int status_delete;

    delete_id = fork();

    if (delete_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (delete_id == 0) {
        char *argv[] = {"rm", "-r", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/rm", argv);
    }
    wait(&status_delete);

```
![](img/soal1/zip_direktori.png)
![](img/soal1/hewan_yang_dijaga.png)


# Soal 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

## Library Program
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>

```

## Poin A
a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

```
// membuat direktori sesuai format nama
    while (1)
    {
		// untuk mendapatkan waktu unixepoch
        time_t unixEpoch = time(NULL); 

        char formattime[32], location[105], link[50], zipFormat[50], imgFormat[50];
        // date +'%Y-%m-%d_%H:%M:%S'
        
		// 3 line untuk mendapatkan sesuai format
        strftime(formattime, 32, "%Y-%m-%d_%H:%M:%S", localtime(&unixEpoch)); 

		// untuk membuat directori baru sesuai format 
        char *mkdirCommand[]= {"mkdir", formattime, NULL}; 
		// untuk membuat child baru guna mengeksecute program mkdir
        pid = fork(); 
        if (pid < 0) 
            exit(EXIT_FAILURE);
		// untuk mengeksecute program mkdir
        if (pid == 0) 
        {
            execvp(mkdirCommand[0], mkdirCommand);
        }
		// untuk menunggu program mkdir selesai 
        else 
        {
            wait(&status);
        }

```
## Poin B 
b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
```
// FUngsi untuk mendowload 15 gambar sesuai
for(int i = 0; i < 15; i++)
            {
				//Membuat proses anak baru
            	pid = fork();
            	if(pid == 0)
            	{
					//Mendapatkan waktu saat ini dalam format unix epoch
		    	    time_t unixEpoch = time(NULL);
					//format waktu menjadi string yang dapat digunakan sebagai nama file
		            strftime(imgFormat, 32, "%Y-%m-%d_%H:%M:%S", localtime(&unixEpoch));
		            
					//membuat path lokasi file yang akan didownload
		            sprintf(location, "%s/%s", formattime, imgFormat);
					//Membuat link untuk file gambar dengan ukuran acak
		            sprintf(link, "https://picsum.photos/%ld", (unixEpoch % 1000) + 50);
					//Membuat array untuk command yang akan dijalankan oleh execvp
		            char *imgCommand[] = {"wget", "-q", "-O", location, link, NULL};
					//Menjalankan command untuk mendownload file gambar
		            execvp(imgCommand[0], imgCommand);
            	}
				//Membuat proses utama menunggu 5 detik sebelum melanjutkan ke proses berikutnya
                sleep(5);
            }

```
## Poin C
c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

```
// Membuat proses anak baru dengan fork
 pid = fork();
 //Jika proses yang berjalan adalah proses anak
            if(pid == 0)
            {
				// Membuat nama file zip dengan format nama folder dan ekstensi zip
            	sprintf(zipFormat, "%s.zip", formattime);
				
				//Membuat array untuk command yang akan dijalankan oleh execvp
				
                char *zipCommand[] = {"zip", "-r", zipFormat, formattime, NULL};
				//Menjalankan command untuk mengompres folder menjadi file zip
                execvp(zipCommand[0], zipCommand);
            }
            //Menunggu proses anak selesai dijalankan
            while(wait(&status) != pid);
			//Membuat array untuk command penghapusan folder yang telah di-zip
            char *rmCommand[] = {"rm", "-r", formattime, NULL};
			// Menjalankan command untuk menghapus folder
            execvp(rmCommand[0], rmCommand);
```
## Poin D
d Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
``` 
//Membuat proses anak baru dengan fork untuk mengkompilasi program killer.c
 pid = fork();
 // Jika proses yang berjalan adalah proses anak
    if(pid == 0)
    {    
		//Membuat array untuk command kompilasi dengan gcc
        char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
		//Menjalankan command untuk melakukan kompilasi
        execv("/usr/bin/gcc", command);
    }
	//Menunggu proses kompilasi selesai dijalankan
    while(wait(&status) != pid);

//Membuat proses anak baru dengan fork untuk menghapus file killer.c
    pid = fork();
	// Jika proses yang berjalan adalah proses anak
    if (pid == 0) {
		// Membuat array untuk command penghapusan file killer.c
    	char *argv[] = {"rm", "killer.c", NULL};
		//Menjalankan command untuk menghapus file
    	execv("/bin/rm", argv);
    }
	//Menunggu proses penghapusan file killer.c selesai dijalankan
    while(wait(&status) != pid);
```
## Poin E
e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
```
if (strcmp(argv[1], "-a") == 0) {
        char *input= ""
        "#include <unistd.h>\n"
        "#include <wait.h>\n"
        "int main() {\n"
            "pid_t child = fork();\n"
            "if (child == 0) {\n"
                "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n"
                "execv(\"/usr/bin/pkill\", argv);\n"
            "}\n"
            "while(wait(NULL) > 0);\n"
            "char *argv[] = {\"rm\", \"killer\", NULL};\n"
            "execv(\"/bin/rm\", argv);\n"
        "}\n";
        fprintf(fileptr, input, sid);
    }

    if (strcmp(argv[1], "-b") == 0) {
        char *input = ""
        "#include <unistd.h>\n"
        "#include <wait.h>\n"
        "int main() {\n"
            "pid_t child = fork();\n"
            "if (child == 0) {\n"
                "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n"
                "execv(\"/bin/kill\", argv);\n"
            "}\n"
            "while(wait(NULL) > 0);\n"
            "char *argv[] = {\"rm\", \"killer\", NULL};\n"
            "execv(\"/bin/rm\", argv);\n"
        "}\n";
        fprintf(fileptr, input, getpid());
    }
```

	Catatan :
Tidak boleh menggunakan system()
Proses berjalan secara daemon
Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)



# Soal 4
 Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnyaUntuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
 - Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
 - Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
 - Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
 - Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
 - Bonus poin apabila CPU state minimum.
 
 Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

 ## Core Program
 ```c
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>
#include <string.h>

 int main(int argc, char *argv[])
{	
    // deklarasi variabel, untuk menyimpan jam, menit, detik. dan flag 
    // ketika jam / menit / detik adalah asterisk
	int jamIsAst = 0;
	int jam;
	int menitIsAst = 0;
	int menit;
	int detikIsAst = 0;
	int detik;
	

    // baca argv, jika merupakan asterisk, maka variable jamIsAst = 1
    // else maka ubah ke integer dan masukkan ke varabel jam
    // lakukan hal yang sama pada menit dan detik
	if(strcmp(argv[1], "*") == 0){
		jamIsAst=1;
	} else {
		jam=atoi(argv[1]);
	}
	

	if(strcmp(argv[2], "*") == 0){
		menitIsAst=1;
	} else {
		menit=atoi(argv[2]);
	}
	
	if(strcmp(argv[3], "*") == 0){
		detikIsAst=1;
	} else {
		detik=atoi(argv[3]);
	}
	
    // variabel untuk menyimpan script path
	char *scriptPath = argv[4];
	

    // looping utama
	while(1){
		// variabel now untuk menyimpan current time pada bentuk time_t
        // lt untuk menyimpan current time pada bentuk struct tm
		time_t now;
		struct tm* lt;
		
		now = time(NULL);
		lt = localtime(&now);
		
        // cek apakah jam sekarang memenuhi konfigurasi cron
		if((jamIsAst || lt->tm_hour == jam) && (menitIsAst || lt->tm_min == menit) && (detikIsAst || lt->tm_sec == detik)){
			// jika memenuhi konfigurasi kron maka,
            // buat child process untuk menjalankan script
            pid_t child_id;
			child_id = fork();
			
			if(child_id==0){
				execlp("sh","sh", scriptPath,NULL);
			}
		}
        // lakukan looping tiap detik	
		sleep(1);
	}
	
}
 ```
Algoritma:
1. baca dan proses argumen argv, lalu masukkan ke variabel sesuai dengan jenisnya
2. dapatkan current time
3. cek apakah current time memenuhi konfigurasi cron
4. jika ya, buat child process. pada child process eksekusi script
5. lkembali ke langkah ke 2, lakukan looping tiap 1 detik

## Error Detection
```c
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>
#include <string.h>

int errArgc(int argc){
	if(argc < 5){
		printf("ERROR: ARGUMEN TERLALU SEDIKIT\n");
		return 1;
	} 
	
	if(argc > 5){
		printf("ERROR: ARGUMEN TERLALU BANYAK\n");
		return 1;
	}
	
	return 0;
}

// fungsi untuk mengecek jumlah argumen
int errArgc(int argc){
    // jumlah argumen harus = 5, yaitu: nama program, jam, menit, detik, dan script path

    // jika jumlah argumen kurang dari 5
	if(argc < 5){
		printf("ERROR: ARGUMEN TERLALU SEDIKIT\n");
		return 1;
	} 
	
    // jika jumlah argumen lebih dari 5
	if(argc > 5){
		printf("ERROR: ARGUMEN TERLALU BANYAK\n");
		return 1;
	}
	
	return 0;
}

// fungsi untuk mengecek format argv
int errArgv(char *argv[]){
	int isError = 0;
	char* argJam = argv[1];
	
    // cek apakah jam merupakan asterisk, jika tidak cek apakah jam dalam bentuk integer.
	if(!(strcmp(argJam, "*")==0)){
		char *endptrJam;
		long num = strtol(argJam, &endptrJam, 10);
		
		if(!(*endptrJam == '\0')){
			printf("ERROR: INVALID JAM\n");
			isError= 1;
		} 
	} 	
	
    // lakuakan pengecekan pada argumen menit, sama seperti pada argumen jam
	char* argMenit = argv[2];
	
	if(!(strcmp(argMenit, "*")==0)){
		char *endptrMenit;
		long num = strtol(argMenit, &endptrMenit, 10);
		
		if(!(*endptrMenit == '\0')){
			printf("ERROR: INVALID MENIT\n");
			isError = 1;
		} 
	} 
	
    // lakuakan pengecekan pada argumen detik sama seperti pada argumen jam
	char* argDetik = argv[3];
	
	if(!(strcmp(argDetik, "*")==0)){
		char *endptrDetik;
		long num = strtol(argDetik, &endptrDetik, 10);
		
		if(!(*endptrDetik == '\0')){
			printf("ERROR: INVALID DETIK\n");
			isError = 1;
		} 
	} 
	
	return isError;
}

// fungsi untuk mengecek apakah script path exist
int scriptPathExist(char* scriptPath){
	return (access(scriptPath, F_OK) == 0);
}


// fungsi untuk mengecek error
int error(char *argv[], int jam, int menit, int detik, char *scriptPath){
	int isError = 0;
	
    // cek format argumen
	if(errArgv(argv)){
		isError = 1;
	}
	
    // cek apakah jam diluar range 0-23
	if(jam < 0 || jam > 23){
		isError = 1;
		printf("ERROR: JAM DI LUAR JANGKAUAN\n");
	}
	
    // cek apakah menit di luar range 0-60
	if(menit < 0 || menit > 59){
		isError = 1;
		printf("ERROR: MENIT DI LUAR JANGKAUAN\n");
	}
	
    // cek apakah mdetik diluar range 0-60
	if(detik < 0 || detik > 59){
		isError = 1;
		printf("ERROR: DETIK DI LUAR JANGKAUAN\n");
	}
	
    // cek apakah script path exist
	if (!scriptPathExist(scriptPath)){
		isError = 1;
		printf("ERROR: SCRIPT PATH TIDAK DITEMUKAN\n");
	}
	
	return isError;
}


int main(int argc, char *argv[])
{	
    // Cek jumlah argumen
	if(errArgc(argc)){
		return 0;
	}
	
    // cek argumen
	if(error(argv, jam, menit, detik, scriptPath)){
		return 0;
	}
}
 ```
Prosedur Error Detection
1. Cek jumlah argumen
2. cek format argumen
3. cek range jam, menit, detik
4. cek script path


## Daemon Process
 ```c
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>
#include <string.h>

int main(int argc, char *argv[])
{	
	pid_t pid, sid;
	pid = fork();
	
	if(pid < 0){
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0){
		exit(EXIT_SUCCESS);
	}
	
	umask(0);
	
	sid = setsid();
	if(sid<0){
		exit(EXIT_FAILURE);
	}
	
	if((chdir("/"))<0){
		exit(EXIT_FAILURE);
	}
	
	printf("Daemon Process is running on PID %d\n", sid);
	
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	
	while(1){
		// core program
        sleep(1);
	}
	
}
 ```

## Final Source Code
```c
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>
#include <string.h>

// fungsi untuk mengecek jumlah argumen
int errArgc(int argc){
	if(argc < 5){
		printf("ERROR: ARGUMEN TERLALU SEDIKIT\n");
		return 1;
	} 
	
	if(argc > 5){
		printf("ERROR: ARGUMEN TERLALU BANYAK\n");
		return 1;
	}
	
	return 0;
}

// fungsi untuk mengecek argv
int errArgv(char *argv[]){
	int isError = 0;
	char* argJam = argv[1];
	
	if(!(strcmp(argJam, "*")==0)){
		char *endptrJam;
		long num = strtol(argJam, &endptrJam, 10);
		
		if(!(*endptrJam == '\0')){
			printf("ERROR: INVALID JAM\n");
			isError= 1;
		} 
	} 	
	
	char* argMenit = argv[2];
	
	if(!(strcmp(argMenit, "*")==0)){
		char *endptrMenit;
		long num = strtol(argMenit, &endptrMenit, 10);
		
		if(!(*endptrMenit == '\0')){
			printf("ERROR: INVALID MENIT\n");
			isError = 1;
		} 
	} 
	
	char* argDetik = argv[3];
	
	if(!(strcmp(argDetik, "*")==0)){
		char *endptrDetik;
		long num = strtol(argDetik, &endptrDetik, 10);
		
		if(!(*endptrDetik == '\0')){
			printf("ERROR: INVALID DETIK\n");
			isError = 1;
		} 
	} 
	
	return isError;
}

// fungsi untuk mengecek file
int scriptPathExist(char* scriptPath){
	return (access(scriptPath, F_OK) == 0);
}


// fungsi untuk mengecek error
int error(char *argv[], int jam, int menit, int detik, char *scriptPath){
	int isError = 0;
	
	if(errArgv(argv)){
		isError = 1;
	}
	
	if(jam < 0 || jam > 23){
		isError = 1;
		printf("ERROR: JAM DI LUAR JANGKAUAN\n");
	}
	
	if(menit < 0 || menit > 59){
		isError = 1;
		printf("ERROR: MENIT DI LUAR JANGKAUAN\n");
	}
	
	if(detik < 0 || detik > 59){
		isError = 1;
		printf("ERROR: DETIK DI LUAR JANGKAUAN\n");
	}
	
	if (!scriptPathExist(scriptPath)){
		isError = 1;
		printf("ERROR: SCRIPT PATH TIDAK DITEMUKAN\n");
	}
	
	return isError;
}


int main(int argc, char *argv[])
{	

	if(errArgc(argc)){
		return 0;
	}
	
	int jamIsAst = 0;
	int jam;
	int menitIsAst = 0;
	int menit;
	int detikIsAst = 0;
	int detik;
	
	if(strcmp(argv[1], "*") == 0){
		jamIsAst=1;
	} else {
		jam=atoi(argv[1]);
	}
	
	if(strcmp(argv[2], "*") == 0){
		menitIsAst=1;
	} else {
		menit=atoi(argv[2]);
	}
	
	if(strcmp(argv[3], "*") == 0){
		detikIsAst=1;
	} else {
		detik=atoi(argv[3]);
	}
	
	char *scriptPath = argv[4];
	
	if(error(argv, jam, menit, detik, scriptPath)){
		return 0;
	}
	
	pid_t pid, sid;
	
	pid = fork();
	

	if(pid < 0){
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0){
		exit(EXIT_SUCCESS);
	}
	
	umask(0);
	
	sid = setsid();
	if(sid<0){
		exit(EXIT_FAILURE);
	}
	
	if((chdir("/"))<0){
		exit(EXIT_FAILURE);
	}
	
	printf("Daemon Process is running on PID %d\n", sid);
	
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	
	while(1){
		
		time_t now;
		struct tm* lt;
		
		now = time(NULL);
		lt = localtime(&now);
		
		if((jamIsAst || lt->tm_hour == jam) && (menitIsAst || lt->tm_min == menit) && (detikIsAst || lt->tm_sec == detik)){
			pid_t child_id;
			child_id = fork();
			
			if(child_id==0){
				execlp("sh","sh", scriptPath,NULL);
			}
		}	
		sleep(1);
	}
	
}
``` 

## Demo Program
Script "script1.sh"
```bash
#!/bin/bash
filename="$(date +%Y-%m-%d_%H:%M:%S).txt"

touch ~/Desktop/crontest/$filename
```
### Normal run (without asterisk)
terminal:

![](./img/soal4/ss-terminal-normal.png)

Before:

![](./img/soal4/ss-output-normal-before.png)

After:

![](./img/soal4/ss-ouput-normal-after.png)

### Normal run (with asterisk)
terminal:

![](./img/soal4/ss-terminal-normal-with-ast.png)

before:

![](./img/soal4/ss-output-normal-with-ast-before.png)

after:

![](./img/soal4/ss-output-normal-with-ast-after-1.png)

![](./img/soal4/ss-output-normal-with-ast-after-2.png)

![](./img/soal4/ss-output-normal-with-ast-after-3.png)

![](./img/soal4/ss-output-normal-with-ast-after-4.png)

![](./img/soal4/ss-output-normal-with-ast-after-5.png)

### Error Detection
Jumlah argumen error:

![](./img/soal4/ss-error-jumlah-arg.png)

Format Argumen error:

![](./img/soal4/ss-error-format-arg.png)

Script not found error:

![](./img/soal4/ss-error-no-script-path.png)



# kendala

No. 1: Sempat tidak mau berjalan programnya

No. 2: Selama praktikum masih error untuk run program membuat direktori sesuai format soal

No. 4: Kendala saat error, program daemon tidak keluar

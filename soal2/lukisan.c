

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>





int main(int argc, char* argv[]) {
    // membuat child proses 
    pid_t pid = fork(); // membuat integer dengan nama pid_t, dengan nilai pid sama dengan fork
    // 
    int status;
    if (pid < 0)// untuk mencegah program dari berjalan dari error
        exit(EXIT_FAILURE);
    if (pid > 0) // jika lebih 0 maka child proses sukses di buat, maka dia akan mereturn pid ke parent proces dan karena positif parent nya akan exit dan child procces akan menjadi daemon procces 
        exit(EXIT_SUCCESS);

    umask(0); // berffungsi mengubah mode aksesnya 
    // Membuat sesion untuk child proccessnya
    pid_t sid = setsid();
    if (sid < 0) // untuk mencegah program dari berjaln dari error
        exit(EXIT_FAILURE);
        
    FILE *fileptr = fopen("killer.c", "w");
 
    if (strcmp(argv[1], "-a") == 0) {
        char *input= ""
        "#include <unistd.h>\n"
        "#include <wait.h>\n"
        "int main() {\n"
            "pid_t child = fork();\n"
            "if (child == 0) {\n"
                "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n"
                "execv(\"/usr/bin/pkill\", argv);\n"
            "}\n"
            "while(wait(NULL) > 0);\n"
            "char *argv[] = {\"rm\", \"killer\", NULL};\n"
            "execv(\"/bin/rm\", argv);\n"
        "}\n";
        fprintf(fileptr, input, sid);
    }

    if (strcmp(argv[1], "-b") == 0) {
        char *input = ""
        "#include <unistd.h>\n"
        "#include <wait.h>\n"
        "int main() {\n"
            "pid_t child = fork();\n"
            "if (child == 0) {\n"
                "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n"
                "execv(\"/bin/kill\", argv);\n"
            "}\n"
            "while(wait(NULL) > 0);\n"
            "char *argv[] = {\"rm\", \"killer\", NULL};\n"
            "execv(\"/bin/rm\", argv);\n"
        "}\n";
        fprintf(fileptr, input, getpid());
    }
    fclose(fileptr);

    pid = fork();
    if(pid == 0)
    {    
        char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execv("/usr/bin/gcc", command);
    }
    while(wait(&status) != pid);

    pid = fork();
    if (pid == 0) {
    	char *argv[] = {"rm", "killer.c", NULL};
    	execv("/bin/rm", argv);
    }
    while(wait(&status) != pid);
        
    // Mencegah program untuk mengahasilkan output apapun
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // membuat direktori sesuai format nama
    while (1)
    {
        time_t unixEpoch = time(NULL); // untuk mendapatkan waktu unixepoch
        char formattime[32], location[105], link[50], zipFormat[50], imgFormat[50];
        // date +'%Y-%m-%d_%H:%M:%S'
        
        strftime(formattime, 32, "%Y-%m-%d_%H:%M:%S", localtime(&unixEpoch)); // 3 line untuk mendapatkan sesuai format

        char *mkdirCommand[]= {"mkdir", formattime, NULL}; // untuk membuat directori baru sesuai format 
        pid = fork(); // untuk membuat child baru guna mengeksecute program mkdir
        if (pid < 0) 
            exit(EXIT_FAILURE);

        if (pid == 0) // untuk mengeksecute program mkdir
        {
            execvp(mkdirCommand[0], mkdirCommand);
        }
        else // untuk menunggu program mkdir selesai 
        {
            wait(&status);
        }
        
        pid = fork();
        if(pid == 0)
        {
            for(int i = 0; i < 15; i++)
            {
            	pid = fork();
            	if(pid == 0)
            	{
		    	    time_t unixEpoch = time(NULL);
		            strftime(imgFormat, 32, "%Y-%m-%d_%H:%M:%S", localtime(&unixEpoch));
		            
		            sprintf(location, "%s/%s", formattime, imgFormat);
		            sprintf(link, "https://picsum.photos/%ld", (unixEpoch % 1000) + 50);

		            char *imgCommand[] = {"wget", "-q", "-O", location, link, NULL};
		            execvp(imgCommand[0], imgCommand);
            	}
                sleep(5);
            }
            while(wait(&status) > 0);

            pid = fork();
            if(pid == 0)
            {
            	sprintf(zipFormat, "%s.zip", formattime);
                char *zipCommand[] = {"zip", "-r", zipFormat, formattime, NULL};
                execvp(zipCommand[0], zipCommand);
            }
            
            while(wait(&status) != pid);
            char *rmCommand[] = {"rm", "-r", formattime, NULL};
            execvp(rmCommand[0], rmCommand);
        }
        sleep(30); // untuk membuat folder baru tiap 30 detik 
     } 
     return 0; 
 }
 

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>
#include <string.h>

// fungsi untuk mengecek jumlah argumen
int errArgc(int argc){
	if(argc < 5){
		printf("ERROR: ARGUMEN TERLALU SEDIKIT\n");
		return 1;
	} 
	
	if(argc > 5){
		printf("ERROR: ARGUMEN TERLALU BANYAK\n");
		return 1;
	}
	
	return 0;
}

// fungsi untuk mengecek argv
int errArgv(char *argv[]){
	int isError = 0;
	char* argJam = argv[1];
	
	if(!(strcmp(argJam, "*")==0)){
		char *endptrJam;
		long num = strtol(argJam, &endptrJam, 10);
		
		if(!(*endptrJam == '\0')){
			printf("ERROR: INVALID JAM\n");
			isError= 1;
		} 
	} 	
	
	char* argMenit = argv[2];
	
	if(!(strcmp(argMenit, "*")==0)){
		char *endptrMenit;
		long num = strtol(argMenit, &endptrMenit, 10);
		
		if(!(*endptrMenit == '\0')){
			printf("ERROR: INVALID MENIT\n");
			isError = 1;
		} 
	} 
	
	char* argDetik = argv[3];
	
	if(!(strcmp(argDetik, "*")==0)){
		char *endptrDetik;
		long num = strtol(argDetik, &endptrDetik, 10);
		
		if(!(*endptrDetik == '\0')){
			printf("ERROR: INVALID DETIK\n");
			isError = 1;
		} 
	} 
	
	return isError;
}

// fungsi untuk mengecek file
int scriptPathExist(char* scriptPath){
	return (access(scriptPath, F_OK) == 0);
}


// fungsi untuk mengecek error
int error(char *argv[], int jam, int menit, int detik, char *scriptPath){
	int isError = 0;
	
	if(errArgv(argv)){
		isError = 1;
	}
	
	if(jam < 0 || jam > 23){
		isError = 1;
		printf("ERROR: JAM DI LUAR JANGKAUAN\n");
	}
	
	if(menit < 0 || menit > 59){
		isError = 1;
		printf("ERROR: MENIT DI LUAR JANGKAUAN\n");
	}
	
	if(detik < 0 || detik > 59){
		isError = 1;
		printf("ERROR: DETIK DI LUAR JANGKAUAN\n");
	}
	
	if (!scriptPathExist(scriptPath)){
		isError = 1;
		printf("ERROR: SCRIPT PATH TIDAK DITEMUKAN\n");
	}
	
	return isError;
}


int main(int argc, char *argv[])
{	

	if(errArgc(argc)){
		return 0;
	}
	
	int jamIsAst = 0;
	int jam;
	int menitIsAst = 0;
	int menit;
	int detikIsAst = 0;
	int detik;
	
	if(strcmp(argv[1], "*") == 0){
		jamIsAst=1;
	} else {
		jam=atoi(argv[1]);
	}
	
	if(strcmp(argv[2], "*") == 0){
		menitIsAst=1;
	} else {
		menit=atoi(argv[2]);
	}
	
	if(strcmp(argv[3], "*") == 0){
		detikIsAst=1;
	} else {
		detik=atoi(argv[3]);
	}
	
	char *scriptPath = argv[4];
	
	if(error(argv, jam, menit, detik, scriptPath)){
		return 0;
	}
	
	pid_t pid, sid;
	
	pid = fork();
	

	if(pid < 0){
		exit(EXIT_FAILURE);
	}
	
	if(pid > 0){
		exit(EXIT_SUCCESS);
	}
	
	umask(0);
	
	sid = setsid();
	if(sid<0){
		exit(EXIT_FAILURE);
	}
	
	if((chdir("/"))<0){
		exit(EXIT_FAILURE);
	}
	
	printf("Daemon Process is running on PID %d\n", sid);
	
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	
	while(1){
		
		time_t now;
		struct tm* lt;
		
		now = time(NULL);
		lt = localtime(&now);
		
		if((jamIsAst || lt->tm_hour == jam) && (menitIsAst || lt->tm_min == menit) && (detikIsAst || lt->tm_sec == detik)){
			pid_t child_id;
			child_id = fork();
			
			if(child_id==0){
				execlp("sh","sh", scriptPath,NULL);
			}
		}	
		sleep(1);
	}
	
}
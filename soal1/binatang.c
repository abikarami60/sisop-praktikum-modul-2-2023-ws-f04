#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>


int main(){
    int stat_download;
    pid_t Downloadid = fork();
    
    if (Downloadid < 0){
        exit(EXIT_FAILURE);
    }
    if (Downloadid == 0){
        char *argv[] = {"wget", "-qO", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "&", NULL};
        execv("/usr/bin/wget", argv);
    }
    waitpid(Downloadid, &stat_download, 0);

    int stat_unzip;
    pid_t unzipid = fork();
    
    if(unzipid == 0){
        char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    waitpid(unzipid, &stat_unzip, 0);
    
    char dir_path[]= ".";
    struct dirent *dp;
    char *path[100];
    DIR *dir = opendir(dir_path);
    
    if (!dir)
      return 0;

    int i = 0;
     while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
        if (dp->d_type == DT_REG)
        {
          char*token = dp->d_name;
          path[i] = token;
          i++;
        }
    }
    }


        // printf("%ld -> %s -> %ld -> %d -> %d\n", dp->d_ino, dp->d_name, dp->d_off, dp->d_reclen, dp->d_type);
    
    int pilih = rand() % sizeof(*path);

    printf("Hewan Yang Dijaga : %s \n", strtok(path[pilih], "."));
    FILE *file;
    file = fopen("penjaga.txt", "w");
    fprintf(file, "Hewan Yang Dijaga : %s", strtok(path[pilih], "."));
    fclose(file);


    closedir(dir);

    // ==GRUP===================================

    pid_t grupid;
    int status_grup;

    grupid = fork();

    if (grupid < 0) {
        exit(EXIT_FAILURE);
    }

    if (grupid == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/mkdir", argv);
    }
    waitpid(grupid, &status_grup, 0);

    // ==PEMILAHAN===================================
    
    DIR *dirr = opendir(".");
    pid_t pilahid;
    int status_pilah;

    while ((dp = readdir(dirr)) != NULL) {
                
        if (dp->d_type == DT_REG) {
            if (strstr(dp->d_name, "darat") != NULL) {
                pilahid = fork();

                if (pilahid < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilahid == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanDarat", NULL};
                    execv("/bin/mv", argv);
                }
                waitpid(pilahid, &status_pilah, 0);
            }

            if (strstr(dp->d_name, "amphibi") != NULL) {
                pilahid = fork();

                if (pilahid < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilahid == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanAmphibi", NULL};
                    execv("/bin/mv", argv);
                }
                waitpid(pilahid, &status_pilah, 0);
            }

            if (strstr(dp->d_name, "air") != NULL) {
                pilahid = fork();

                if (pilahid < 0) {
                    exit(EXIT_FAILURE);
                }

                if (pilahid == 0) {
                    char *argv[] = {"mv", dp->d_name, "HewanAir", NULL};
                    execv("/bin/mv", argv);
                }
                waitpid(pilahid, &status_pilah, 0);
            }
        }
    }
    closedir(dirr);

    

    pid_t zipid;
    int status_zip;

    // 1. Darat
    zipid = fork();

    if (zipid < 0) {
        exit(EXIT_FAILURE);
    }

    if (zipid == 0) {
        char *argv[] = {"zip", "-qr", "HewanDarat.zip", "HewanDarat", NULL};
        execv("/bin/zip", argv);
    }
    waitpid(zipid, &status_zip, 0);

    // 2. Amphibi
    zipid = fork();

    if (zipid < 0) {
        exit(EXIT_FAILURE);
    }

    if (zipid == 0) {
        char *argv[] = {"zip", "-qr", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/bin/zip", argv);
    }
    waitpid(zipid, &status_zip, 0);

    // 3. Air
    zipid = fork();

    if (zipid < 0) {
        exit(EXIT_FAILURE);
    }

    if (zipid == 0) {
        char *argv[] = {"zip", "-qr", "HewanAir.zip", "HewanAir", NULL};
        execv("/bin/zip", argv);
    }
    waitpid(zipid, &status_zip, 0);

    // ==DELETE===================================
    
    pid_t deleteid;
    int status_delete;

    deleteid = fork();

    if (deleteid < 0) {
        exit(EXIT_FAILURE);
    }

    if (deleteid == 0) {
        char *argv[] = {"rm", "-r", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
        execv("/bin/rm", argv);
    }
    waitpid(deleteid, &status_delete, 0);
}
